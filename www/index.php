<?php
ini_set("default_charset", "UTF-8");
mb_internal_encoding("UTF-8");
setlocale(LC_ALL, 'ru_RU.UTF-8');
setlocale(LC_NUMERIC, 'C');
session_start();

require_once $_SERVER['DOCUMENT_ROOT'].'/config.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/usersApi.php';

/*
// Примеры запросов к API:

// Создание + редактирование + выборка:
{"messages":[
{"action":"create","data":{"login":"samodelkeen","pass":"12345","fio":"Егоров Владимир Викторович","post":"программист"}},
{"action":"update","data":{"login":"samodelkeen2","post":"программист PHP"}},
{"action":"list","data":{"page":1,"size":10,"filter":{"fio":"Владим"}}}
]}

// Выборка:
{"messages":[
{"action":"list","data":{"page":1,"size":10,"filter":{"fio":"Владим"}}},
{"action":"list","data":{"page":1,"size":10,"filter":{"login":"neo","fio":"пользователь","post":"шаблон"}}}
]}

// Удаление:
{"messages":[
{"action":"delete","data":{"login":"samodelkeen"}}
]}


// Примеры ответов API:
{"answers":[
{"result":"OK","data":{"message":""}},
{"result":"ERROR","data":{"message":"login samodelkeen2 не найден"}},
{"result":"OK","data":{"list":[{"login":"samodelkeen","fio":"Егоров Владимир Викторович","post":"программист"}]}}
]}

*/

$result = '';

if (!empty($_POST)) {
	$fstring = date('Y-m-d H:i:s') . "\n";
	$fstring .= 'REMOTE_ADDR: ' . $_SERVER['REMOTE_ADDR'] . "\n";
	$fstring .= 'HTTP_REFERER: ' . (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'домен не определен') . "\n";

	if (isset($_POST['usrdata']) && !empty($_POST['usrdata'])) {
		
		/* Вариант с обращением к методу ProcessJson() - передача API пакета в формате JSON */
		$resultJson = UsersApi::GetInstance() -> ProcessJson($_POST['usrdata']);
		switch (UsersApi::GetInstance() -> status) {
			case 'STORAGE_CONNECT_ERROR':
			case 'JSON_PARSE_ERROR':
				$result = json_encode(array('API_ERROR' => UsersApi::GetInstance() -> status));
				$result .= '<br>' . '<b>Сообщение сервера: </b>' . UsersApi::GetInstance() -> message;
				$fstring .= UsersApi::GetInstance() -> message . "\n";
				break;
			default:
				$result = '{"API_OK":' . $resultJson . '}';
		}

		/* Вариант с разбором JSON-пакета и раздельного обращения к соответствующим методам API */
		// TODO: Сделать. Не успел реализовать.

	} elseif (isset($_POST['tstdata']) && !empty($_POST['tstdata'])) { // если POST-запрос наш (прочие тестовые сообщения):
		$fstring .= "Тестовый POST-запрос к приложению\n";
		$buff = '';
		foreach ($_POST as $key => $val) {
			$buff .= "$key: $val" . "\n";
		}
		$fstring .= $buff;
	} else { // если POST-запрос "левый":
		$fstring .= "\"Левый\" POST-запрос к сайту\n";
		$buff = '';
		foreach ($_POST as $key => $val) {
			$buff .= "$key: $val" . "\n";
		}
		$fstring .= $buff;
	}

} elseif (!empty($_GET)) {
	$fstring = date('Y-m-d H:i:s') . "\n";
	$fstring .= 'REMOTE_ADDR: ' . $_SERVER['REMOTE_ADDR'] . "\n";
	$fstring .= 'HTTP_REFERER: ' . (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'домен не определен') . "\n";
	$fstring .= "GET-запрос к сайту\n";
	$buff = '';
	foreach ($_GET as $key => $val) {
		$buff .= "$key: $val" . "\n";
	}
	$fstring .= $buff;
}

if (isset($fstring)) {
	$fstring .= '---' . "\n";
	$f = fopen(LOGS_PATH . '/users_api.log', 'a+');
	fwrite($f, $fstring);
	fclose($f);
}

session_destroy();
?>

<?php
include "index.html";
?>

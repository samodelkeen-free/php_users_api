<?php

class XmlDataStorage extends DataStorage {

	private $dom;
	private $filename;

    protected function openDb($params) {
		try {
			$this->filename = $_SERVER['DOCUMENT_ROOT'] . '/' . $params['path'];
			$this->dom = new DomDocument();
			$this->dom->load($this->filename);
			if (!$this->dom) {
				$this->message = "Не удалось подключиться к XML-файлу хранилища данных";
			} else {
				$this->connected = true;
			}
		} catch (Exception $e) {
			$this->message = "Не удалось подключиться к XML-файлу хранилища данных: " . $e->getMessage();
		}
    }

	protected function closeDb() {
	}

	protected function escapeString($str) {
		return $str;
	}

	public function AddUser($data) {
		if (!$this->dataCheck('create', $data)) return false;
		if ( self::findElementByNodeValue($this->dom->getElementsByTagName('user'), 'login', $data['login']) ) {
			$this->status = "USER_ALREADY_EXISTS";
			$this->message = "Не удалось добавить уже существующего пользователя: " . $data['login'];
			return false;
		} else {
			$pass = trim($data['pass']);
			if (!empty($pass)) {
				$pass = DataStorage::GetNewHash($pass, $salt);
			} else $salt = '';
			$this->dataPrepare($data);
			$root = $this->dom->documentElement;
			$newuser = $this->dom->createElement('user');
			$newuser->appendChild($this->dom->createElement("login", $data['login']));
			$newuser->appendChild($this->dom->createElement("pass", $pass));
			$newuser->appendChild($this->dom->createElement("salt", $salt));
			$newuser->appendChild($this->dom->createElement("fio", $data['fio']));
			$newuser->appendChild($this->dom->createElement("post", $data['post']));
			$newuser->appendChild($this->dom->createElement("time", date('Y-m-d H:i:s')));
			$root->appendChild($newuser);
			//echo $this->dom->saveXML();
			$this->dom->save($this->filename);
			return true;
		}
	}

	public function EditUser($data) {
		if (!$this->dataCheck('update', $data)) return false;
		if ( !($element = self::findElementByNodeValue($this->dom->getElementsByTagName('user'), 'login', $data['login'])) ) {
			$this->status = "USER_NOT_EXISTS";
			$this->message = "Пользователь не существует: " . $data['login'];
			return false;
		} else {
			// TODO: Реализовать
			$this->status = "METHOD_NOT_IMPLEMENTED";
			$this->message = "Метод пока не реализован: " . '"update"';
			return false;
		}
	}

	public function DeleteUser($data) {
		if (!$this->dataCheck('delete', $data)) return false;
		if ( !($element = self::findElementByNodeValue($this->dom->getElementsByTagName('user'), 'login', $data['login'])) ) {
			$this->status = "USER_NOT_EXISTS";
			$this->message = "Пользователь не существует: " . $data['login'];
			return false;
		} else {
			$element->parentNode->removeChild($element);
			//echo $this->dom->saveXML();
			$this->dom->save($this->filename);
			return true;
		}
	}

	public function GetUsers($data) {
		$result = [];
		if (!$this->dataCheck('list', $data)) return $result;
		//$query = "//users/user/login[text() = \"". $data['filter']['login'] . "\"]/..";
		//$query = "//users/user[fio[contains(text(), \"" . $data['filter']['fio'] . "\")]]/..";
		//$query = "//users/user/post[contains(text(), \"" . $data['filter']['post'] . "\")]/..";
		//$query .= "[ post[contains(text(), \"" . $data['filter']['post'] . "\")] and fio[contains(text(), \"" . $data['filter']['fio'] . "\")] ]/..";
		$query = '';
		if (isset($data['filter']['login'])) $query .= (empty($query) ? '' : ' and ') . "login[text() = \"". $data['filter']['login'] . "\"]";
		if (isset($data['filter']['fio'])) $query .= (empty($query) ? '' : ' and ') . "fio[contains(text(), \"" . $data['filter']['fio'] . "\")]";
		if (isset($data['filter']['post'])) $query .= (empty($query) ? '' : ' and ') . "post[contains(text(), \"" . $data['filter']['post'] . "\")]";
		if (!empty($query)) {
			$query = "//users/user[" . $query . "]";
			$xpath = new DOMXPath($this->dom);
			$elements = $xpath->query($query);
		} else {
			$elements = $this->dom->getElementsByTagName('user');
		}
		//TODO: Выборка окна записей в соответствии с размером страницы size и номером страницы page.
		foreach ($elements as $element) {
			$row = [];
			foreach ($element->childNodes as $childNode) {
				if ($childNode->nodeType != XML_TEXT_NODE) {
					if ($childNode->nodeName != 'pass' && $childNode->nodeName != 'salt')
					$row[$childNode->nodeName] = $childNode->nodeValue;
				}
			}
			$result[] = $row;
		}
		$this->status = 'OK';
		$this->message = '';
		return $result;
	}

	private static function findElementByNodeValue($elements, $node, $value) {
		$element = false;
		for ($i = 0; $i < $elements->length; ++ $i) {
			foreach ($elements->item($i)->childNodes as $childNode) {
				if ($childNode->nodeName == $node && $childNode->nodeValue == $value) {
					$element = $elements->item($i);
					break;
				}
			}
		}
		return $element;
	}

	private static function findElementByAttrValue($elements, $attr, $value) {
		$element = false;
		for ($i = 0; $i < $elements->length; ++ $i) {
			if ($elements->item($i)->getAttribute($attr) == $value) {
				$element = $elements->item($i);
				break;
			}
		}
		return $element;
	}

}
<?php
require_once USERSAPI_ROOT . 'MySqlDataStorage.php';
require_once USERSAPI_ROOT . 'PostgreSqlDataStorage.php';
require_once USERSAPI_ROOT . 'XmlDataStorage.php';

abstract class DataStorage {

	public $status = '';
	public $message = '';
	public $connected = false;

    function __construct($params) {
		$this->openDb($params);
    }

	function __destruct() {
		$this->closeDb();
	}

	/**
	 * Выбор хранилища в соответствии с файлом конфигурации
	 *
	 */
	public static function SelectStorage($config) {
		
		switch ($config['storage']) {
			case 'xml': return new XmlDataStorage($config['storages']['xml']);
			case 'mysql': return new MySqlDataStorage($config['storages']['mysql']);
			case 'postgresql': return new PostgreSqlDataStorage($config['storages']['postgresql']);
		}
	}

	/* Наследуемые функции класса */

	protected function dataCheck($action, $data) {
		try {
			switch ($action) {
				case "create":
					if (!isset($data['login'])) throw new Exception('Не указан логин');
					if (!isset($data['pass'])) throw new Exception('Не указан пароль');
					if (!isset($data['fio'])) throw new Exception('Не указаны ФИО');
					if (!isset($data['post'])) throw new Exception('Не указана должность');
					break;
				case "update":
					if ( !isset($data['pass']) && !isset($data['fio']) && !isset($data['post']) ) throw new Exception('Не указаны данные для корректировки');
					break;
				case "delete":
					if (!isset($data['login'])) throw new Exception('Не указан логин');
					break;
				case "list":
					if (empty($data)) throw new Exception('Не указаны параметры фильтра');
					if (!isset($data['size'])) throw new Exception('Не указан размер страницы выборки');
					break;
			}
		}
		catch (Exception $e) {
			$this->status = "DATA_ERROR";
			$this->message = "Данные не прошли проверку: " . $e->getMessage();
			return false;
		}
		return true;
	}

	protected function dataPrepare($data) {
		if (isset($data['login'])) $data['login'] = $this->escapeString($data['login']);
		if (isset($data['fio'])) $data['fio'] = $this->escapeString($data['fio']);
	}

	/* Переопределяемые функции класса */

	abstract protected function openDb($params);

	abstract protected function closeDb();

	abstract protected function escapeString($str);

	abstract public function AddUser($data);

	abstract public function EditUser($data);

	abstract public function DeleteUser($data);

	abstract public function GetUsers($data);

	/* Служебные статические функции */

	protected static function getNewHash($keyword, &$salt) {
		$salt = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!?'), 0, 64);
		return hash("sha256", $keyword . $salt);
	}

}
<?php

class PostgreSqlDataStorage extends DataStorage {

	private $db;

    protected function openDb($params) {
		try {
			$this->db = pg_connect( sprintf("host=%s dbname=%s user=%s password=%s", $params['host'], $params['name'], $params['user'], $params['pass'])/*, PGSQL_CONNECT_FORCE_NEW*/ );
			if (!$this->db) {
				$this->message = "Не удалось подключиться к базе данных PostgreSQL";
			} else {
				$this->connected = true;
				pg_set_client_encoding($this->db, "UNICODE");
			}
		} catch (Exception $e) {
			$this->message = "Не удалось подключиться к базе данных PostgreSQL: " . $e->getMessage();
		}
    }

	protected function closeDb() {
		if ($this->connected)
			$this->connected = !pg_close($this->db);
	}

	protected function escapeString($str) {
		return pg_escape_string($this->db, $str);
	}

	public function AddUser($data) {}

	public function EditUser($data) {}

	public function DeleteUser($data) {}

	public function GetUsers($data) {}

}
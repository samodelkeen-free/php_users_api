<?php
if (!defined('USERSAPI_ROOT')) {
    define('USERSAPI_ROOT', dirname(__FILE__) . '/');
    require USERSAPI_ROOT . 'DataStorage.php';
}

class UsersApi {

	private static $instance = null;
	private $config;

	public $storage = null;
	public $status = '';
	public $message = '';

    private function __construct() {
		$this->config = self::config(USERSAPI_ROOT . 'config.php');
		$this->storage = DataStorage::SelectStorage($this->config);
		if ($this->storage->connected) {
		} else {
			$this->message = $this->storage->message;
			$this->status = 'STORAGE_CONNECT_ERROR';
		}
    }

	function __destruct() {
		//$this->storage->close(); Закрывается само...
	}

	private function __clone() {}

	public static function GetInstance() {
		if (is_null(self::$instance))
			self::$instance = new self();
		return self::$instance;
	}

	private function config($file) {
		return include($file);
	}

	/**
	 * Обработка запроса к хранилищу в формате JSON
	 *
	 */
	public function ProcessJson($data) {
		$resultArray = [];
		$dataArray = json_decode($data, true);
		if (is_array($dataArray)) {
			foreach ($dataArray['messages'] as $key => $value) {
				switch ($value['action']) {
					case "create":
					case "update":
					case "delete":
						$result = $this->UpdateUser($value['action'], $value['data']);
						$result = array('result' => $this->status, 'data' => array('message' => $this->message));
					break;
					case "list":
						$result = $this->GetUsers($value['data']);
						$result = array('result' => $this->status, 'data' => array('list' => $result, 'message' => $this->message));
					break;
					default:
						$this->status = 'ACTION_ERROR';
						$this->message = "Нет обработчика для: " . $value['action'];
						$result = array('result' => $this->status, 'data' => array('message' => $this->message));
				}
				$resultArray[] = $result;
			} // foreach ($dataArray['messages'] as $key => $value)
		} else {
			$this->status = 'JSON_PARSE_ERROR';
			$this->message = "Не удается распарсить JSON-строку:\n" . $data;
		}
		
		return json_encode( array('answers' => $resultArray) );
	}

	/**
	 * Корректировка данных о пользователе в хранилище
	 *
	 */
	public function UpdateUser($action, $data) {
		$this->status = '';
		$this->message = '';
		switch ($action) {
			case "create":
				$result = $this->storage->AddUser($data);
			break;
			case "update":
				$result = $this->storage->EditUser($data);
			break;
			case "delete":
				$result = $this->storage->DeleteUser($data);
			break;
			default:
				$result = false;
				$this->status = 'ACTION_ERROR';
				$this->message = "Нет обработчика для: " . $action;
		}
		if ($result) {
			$this->status = 'OK';
			$this->message = '';
		} else {
			if (empty($this->status)) { 
				$this->status = $this->storage->status;
				$this->message = $this->storage->message;
			}
		}
		return $result;
	}		

	/**
	 * Получение данных о пользователях из хранилища
	 *
	 */
	public function GetUsers($data) {
		$result = $this->storage->GetUsers($data);
		$this->status = $this->storage->status;
		$this->message = $this->storage->message;
		return $result;
	}

	// Поиск в двумерном массиве
	public static function multi_search($value, $array, $key, $level = 0, $num = 0) {
	  $results = array();
      if (is_array($array)) {
		if (isset($array[$key]) && $array[$key] == $value) $results[] = array('level'=>$level, 'num'=>$num, 'array'=>$array);
		foreach ($array as $nn=>$subarray) $results = array_merge($results, self::multi_search($value, $subarray, $key, $level + 1, $nn));
      }
      return $results;
	}

}
<?php

class MySqlDataStorage extends DataStorage {

	private $db;

    protected function openDb($params) {
		try {
			$this->db = new mysqli($params['host'], $params['user'], $params['pass'], $params['name']);
		} catch (Exception $e) {}
		if ($this->db->connect_errno) {
			$this->message = "Не удалось подключиться к базе данных MySQL: (" . $this->db->connect_errno . ") " . $this->db->connect_error;
		} else {
			$this->connected = true;
			$this->db->autocommit(true);
			$this->db->set_charset('utf8');
		}
    }

	protected function closeDb() {
		if ($this->connected)
			if ($this->db->close()) $this->connected = false;
	}

	protected function escapeString($str) {
		return $this->db->real_escape_string($str);
	}

	public function AddUser($data) {}

	public function EditUser($data) {}

	public function DeleteUser($data) {}

	public function GetUsers($data) {}

}